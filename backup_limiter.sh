#!/bin/bash
#
#  sholan for r0x.fr
#  v.0.1        2015-03-12      Creating a script to allow limiting number of backups
#                               on minecraft HH VPS (disk space saturation issues)
#  v.0.2	    2015-04-15		Modifying script to only count and delete folders 
#								(not regular files)
#


# backup folder path (the one containing files with ISO Date format name)
# example : default for minecraft HH VPS : /home/minecraft_backups/world
mc_path=/home/minecraft_backups/worlds
# maximum number of backup folders (10 = 10 backup folders)
max_backups=10
# minimum number of backup folders (3 = 3 backup folders) (not used in this version)
min_backups=3
# maximal percentage of fs size used (not used in this version)
max_fs_pct=85

# change directory to $mc_path (relative paths used after)
cd $mc_path

# Count number of folders present
count_bu=`ls -1d */ | wc -l `

# verif if this number is over limit
if [ $count_bu -gt $max_backups ] 
then 
	# if greater than limit, remove the oldest (number of folders present - max number) folders 
	rm -rf `ls -1d */ | head -n $((count_bu - max_backups))`
else
	echo "Number of backups in directory is conforming maximum value set"
fi

# getting mc_path fs size in numeric format (unit, kilo bytes)
size_fs=$(df -Bk --output=size $mc_path | tail -n 1 | sed 's/K//')

# converting max_fs_pct into numeric format (unit, kilo bytes)
max_fs=$(( size_fs * max_fs_pct /100))

# for debug purpose
echo "Minecraft Backup FileSystem total size : $size_fs (kB)."
echo "Max Size that can be used : $max_fs (kB), for $max_fs_pct % set." 

# getting used fs size in numeric format (unit, kilo bytes)
fsused=$(df -Bk --output=used $mc_path | tail -n 1 | sed 's/K//')
echo "Minecraft Backup FileSystem used size : $fsused (kB)."

# verify if current occupied size is above set limit
if [ $fsused -gt $max_fs ] ; then
	folders_list=
	folders_sum_size=0
	safety=0
	num_folders_del=0
	
	### detecting if suppressing backups would lead to a sufficient amount of 
	### freed disk space
	
	# calculating FS size of our backups from oldest to newest
	for bups in $mc_path/* ; do
		# only process directories
		if [ -d "$bups" ]; then
			curr_size=$(du -sBk $bups | cut -f 1 | sed 's/K//')
			folders_sum_size=$((folders_sum_size+curr_size))
			folders_list="$folders_list$bups "
			
			# this control is pretty fucked up ATM (can't understand what I tried to do, will figure it out later)
			if [ $folders_sum_size -gt $fsused-$max_fs ]; then
				safety=1
				break
			fi
		fi
	done
	
	# would be used to alert that FS saturation is not caused by backups
	# (got to determine if 2 tests are really needed once line 68 usage is figured out)
	if [ $safety -neq 1 ] ; then
		echo "Security issue, aborting : filesystem is heavily saturated"
		exit 1
	fi
	if [ $min_backups -gt $count_bu-$num_folders_del ]; then
		echo "Filesystem saturated by something else than MC backups"
		exit 2
	fi
else
	echo "Filesystem is not overloaded"
fi

exit 0
