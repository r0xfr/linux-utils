#! /bin/bash

#####
#
# Auteur : o_be_one
# Date : 2015-04-27
# Objectif : supprimer les backups plus vieux que la date définit, tout en conservant les backups du 1er des mois définis également
# Usage : placer en crontab, configurer les variables
#
#####


### A editer :

FOLDER="/backups/database"; # Quel est le dossier où faire la rotation ?
KEEPMONTHLY=5; # On conserve le backup du premier jour du mois sur combien de mois ?
KEEPDAILY=4; # On conserver combien de backups journaliers ?i
LOCKFILE="/var/run/backups-delete.lock"; # Définition d'un fichier pour empêcher une double exécution

### Editer ce qui suit à vos risques !

####
# find /backups/database/ -mtime -3 -printf '%Cd#%p\n' |awk -F'#' '$1 !~ /^01/{print $2;}'
# trouvé sur http://superuser.com/questions/559224/removing-files-older-then-1-month-but-leave-files-created-at-the-1st-day-of-the
####

## Est-ce que le script roule présentement ?
if [ -f "$LOCKFILE" ]; then
        echo "Script est en cours d'exécution. Si il s'agit d'une erreur, supprimez $LOCKFILE.";
        exit 1;
else
        touch $LOCKFILE;
fi

## Gestion des logs :

NOW=$(date +"%d-%m-%Y")
TIMENOW=$(date +"%T")
echo "Dernière exécution du script : $NOW à $TIMENOW" > /var/log/backups-delete.log;
echo >> /var/log/backups-delete.log;
echo "Suppression(s) du/des jour(s) :" >> /var/log/backups-delete.log;

## Suppression(s) quotidiennes :

# on corrige pour avoir un find juste :
KEEPDAILY=$((KEEPDAILY -1))

# on recherche en ignorant le dossier daily, via un indicateur de jour on génère la liste de fichiers à supprimer :
find $FOLDER -mtime +$KEEPDAILY \( ! -iname "daily" \) -printf '%Cd#%p\n' |awk -F'#' '$1 !~ /^01/{print $2;}' >> /var/log/backups-delete_exe.log;
cat /var/log/backups-delete_exe.log >> /var/log/backups-delete.log;

# on supprime le contenu indiqué dans le fichier crée
for i in `cat /var/log/backups-delete_exe.log`
do
        rm $i;
done

# on supprime le fichier crée
rm /var/log/backups-delete_exe.log;

## Suppression mensuelle :

# on change les mois en jour (erreur possible à 1 mois près)
KEEPMONTHLY=$((KEEPMONTHLY *30))

# on ignore le dossier daily et on supprime le bordel
echo >> /var/log/backups-delete.log;
echo "Suppression(s) du/des mois :" >> /var/log/backups-delete.log;
find $FOLDER -mtime +$KEEPMONTHLY \( ! -iname "daily" \) -exec rm {} \; >> /var/log/backups-delete.log

# le script a finit, on supprime le lock
rm -f $LOCKFILE