#! /bin/bash

# # Variables a editer !
EXEC_PATH="/root/projectzomboid"
PROC_NAME="zomboid"

# # Ne plus rien toucher a partir d'ici sauf si vous vous y connaissez !
cd $EXEC_PATH
# # check to see if server is already running
PROC_NAME_cnt=$(ps -ef | grep -v grep | grep -c "$PROC_NAME ")
# # if the server isnot already running execute it
if (( $PROC_NAME_cnt == 0 )); then
  echo "$PROC_NAME : le serveur a crash. Redemarrage ..."
  screen -dmS $PROC_NAME sh start.sh
  # # if the server is running, do that
fi